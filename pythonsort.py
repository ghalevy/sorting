#!/usr/bin/python3

import sys
import time
import random
# from functools import lru_cache

my_range = int(sys.argv[1])
num_elements = int(sys.argv[2])

# print(type(my_range))
# print(type(num_elements))

def bubble_sort(array):
    ultimate = len(array) - 1

    def sort_loop(arr):
        sorted = True
        for i in range(0, ultimate):
            if arr[i] > arr[i + 1]:
                sorted = False
                arr[i], arr[i+1] = arr[i + 1], arr[i]

        return sorted
        
    sorted = False
    while not sorted:
        sorted = sort_loop(array)

    return array


def insertion_sort(_list):
    sorted_list = [_list.pop(0)]
    while len(_list) > 0:
        sorted_list.append(_list.pop(0))
        for j in range(len(sorted_list) - 1, 0, -1):
            if sorted_list[j] < sorted_list[j-1]:
                sorted_list[j], sorted_list[j-1] = sorted_list[j-1], sorted_list[j]
                
    return sorted_list


def selection_sort(_list):
    for i in range(len(_list)):
        min_index = i
        for j in range(i + 1, len(_list)):
            if _list[j] < _list[min_index]:
                min_index = j
        _list[i], _list[min_index] = _list[min_index], _list[i]

    return _list

def quick_sort(_list):
    length = len(_list)
    if length <= 1:
        return _list
    else:
        pivot = _list.pop()

    items_greater = []
    items_lower = []

    for i in _list:
        if i < pivot:
            items_lower.append(i)
        else:
            items_greater.append(i)

    return quick_sort(items_lower) + [pivot] + quick_sort(items_greater)



my_array = [random.randint(1, my_range) for i in range(0, num_elements)]  
print("my_array:\n {}".format(my_array))
print()
print()

# bubble sort
start_time = time.time()
bubble_sorted_array = bubble_sort(list(my_array))
print("bubble_sorted:\n {}".format(bubble_sorted_array))
bubble_sort_run_time = time.time() - start_time
print()
print()

# selection sort
start_time = time.time()
selection_sorted_array = selection_sort(list(my_array))
print("selection_sorted:\n {}".format(selection_sorted_array))
selection_sort_run_time = time.time() - start_time
print()
print()

# insertion sort
start_time = time.time()
insertion_sorted_array = insertion_sort(list(my_array))
print("insertion_sorted:\n {}".format(insertion_sorted_array))
insertion_sort_run_time = time.time() - start_time
print()
print()

# quick sort
start_time = time.time()
quick_sorted_array = quick_sort(list(my_array))
print("quick_sorted:\n {}".format(quick_sorted_array))
quick_sort_run_time = time.time() - start_time
print()
print()

# conclusions
if bubble_sorted_array == selection_sorted_array == insertion_sorted_array == quick_sorted_array:
    print("all arrays match")
else:
    print("there was a mismatch")
print()
print()

print("bubble sort took {:.2f} seconds to run".format(bubble_sort_run_time))
print("insertion sort took {:.2f} seconds to run".format(insertion_sort_run_time))
print("selection sort took {:.2f} seconds to run".format(selection_sort_run_time))
print("quick sort took {:.2f} seconds to run".format(quick_sort_run_time))
print()
selection_sort_efficiency = bubble_sort_run_time / selection_sort_run_time
insertion_sort_efficiency = bubble_sort_run_time / insertion_sort_run_time
quick_sort_efficiency = bubble_sort_run_time / quick_sort_run_time
print("selection sort was {:.2f} times more efficient than bubble sort".format(selection_sort_efficiency))
print("insertion sort was {:.2f} times more efficient than bubble sort".format(insertion_sort_efficiency))
print("quick sort was {:.2f} times more efficient than bubble sort".format(quick_sort_efficiency))

